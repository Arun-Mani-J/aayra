"""Contains ContextPopover."""
from urllib.parse import quote_plus

from gi.repository import Adw, Gio, Gtk

from aayra.ext import Settings, Share


def launch_uri_for_text(buffer: Gtk.TextBuffer, url: str):
    """Launch the URI with contents from buffer."""
    text = buffer.get_text(
        *buffer.get_bounds(),
        False,
    )
    content = quote_plus(text)
    uri = url.format(CONTENT=content)
    Gio.AppInfo.launch_default_for_uri(uri)


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/context_popover.ui")
class ContextPopover(Gtk.Popover):
    """ContextPopover presents the selected contents of a reader."""

    __gtype_name__ = "ContextPopover"
    selected_buffer = Gtk.Template.Child()

    @Gtk.Template.Callback()
    def copy_selected(self, _):
        """Copy selected contents."""
        clipboard = self.get_clipboard()
        text = self.selected_buffer.get_text(
            *self.selected_buffer.get_bounds(),
            False,
        )
        clipboard.set(text)

    @Gtk.Template.Callback()
    def search_selected(self, _):
        """Search selected contents."""
        launch_uri_for_text(self.selected_buffer, Settings.SEARCH_URL)

    @Gtk.Template.Callback()
    def read_selected(self, _):
        """Read out selected contents."""
        launch_uri_for_text(self.selected_buffer, Settings.READ_URL)

    @Gtk.Template.Callback()
    def summarize_selected(self, _):
        """Summarize selected contents."""
        launch_uri_for_text(self.selected_buffer, Settings.SUMMARIZE_URL)
