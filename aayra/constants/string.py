"""Contains String."""


class String:
    """Single source of all message strings."""

    ADD_BOOK = "Add Book"

    AUDIO_ROW_DIALOG_TITLE_ADD = "Add an Audiobook"

    AUDIO_ROW_DIALOG_TITLE_EDIT = "Edit an Audiobook"

    DESK_AUDIO_ROW_DIALOGUE_PATH = "Path to track"

    DESK_EBOOK_TITLE = "eBook"

    DESK_EBOOK_SUBTITLE = "Path to eBook"

    DESK_THUMBNAIL_TITLE = "Thumbnail"

    DESK_THUMBNAIL_SUBTITLE = "Path to thumbnail"

    DESK_REMOVE_TITLE = "Remove"

    DESK_REMOVE_DESCRIPTION = "The files are not deleted"

    EDIT_BOOK = "Edit Book"

    FILE_DIALOG_TITLE_EBOOK = "Choose eBook"

    FILE_DIALOG_TITLE_AUDIO = "Choose audiobook"

    FILE_DIALOG_TITLE_AUDIOS = "Choose audiobooks"

    FILE_DIALOG_TITLE_THUMBNAIL = "Choose thumbnail"
