"""Launch Aayra."""

import sys

import fitz

from aayra.app import Aayra

app = Aayra()
app.run(sys.argv)
app.store.close()
