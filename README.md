# Aayra

Aayra is an eBook reader and audiobook player. It aims to be a comfortable, simplistic GUI application that can be used by anyone.

It is still in very early stages.

## Getting Started

As the app is still in development, the steps to try it out is a little complicated. The first goal is finish it for GNU/Linux, after which Windows will be tested.

### Install Dependencies

Aayra is written in Python and uses GTK along with a few GObject libraries for the majority. These should be available in your distribution's package manager.

1. [Python](https://www.python.org)
2. [GTK](https://www.gtk.org)
3. [libadwaita](https://gitlab.gnome.org/GNOME/libadwaita)
4. [GStreamer](https://gstreamer.freedesktop.org/documentation/tutorials/index.html?gi-language=python)
5. [WebKitGTK](https://webkitgtk.org/)
6. [PyGObject](https://pygobject.readthedocs.io/en/latest/)
7. [SQLite](https://www.sqlite.org/)

### Clone Repository

```sh
git clone git@gitlab.com:Arun-Mani-J/aayra.git
```

### Launch

```sh
$ cd aayra
aayra $ ./aayra.sh
```

## Development

### UI

Like any other GTK app, we also make extensive use of GTK UI files. [Cambalache](https://gitlab.gnome.org/jpu/cambalache) is good to design the prototypes. Sometimes, due to limitations of the prototyping tool or so, it is not possible to directly include the `.ui` files. Hence, they are stored in [data/ui/raw](/data/ui/raw). These files are used as reference (which sometimes is just a copy paste) to design the final UI files at [data/ui](/data/ui) (by directly editing the XML).

### Mappings

[state](/state) maps to `$XDG_STATE_HOME`. It contains the opened books database and their thumbnails.

[data](/data) maps to `$XDG_DATA_HOME`. It contains essential data of the app.

### Tests

The app has a lot of components. Each of the can be individually (manually) tested by just executing the file.

```sh
$ cd aayra
aayra $ export AAYRA_DEV=1
aayra $ python3 -m tests.audio
```

The unit tests (automated) can be done similarly.

```sh
$ cd aayra
aayra $ export AAYRA_DEV=1
aayra $ python3 -m tests.unit
```

However, unit tests are yet to be written.

## License

Copyright (C) Forever Arun Mani J, Ashsui Marv and Priyank.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
