"""Contains PagesView."""

from enum import Enum


class PagesView(Enum):
    """Arrangement of pages."""

    SINGLE = "SINGLE"
    DUAL = "DUAL"
