"""Contains Desk."""

from gi.repository import Adw, Gio, GObject, Gst, GstPbutils, Gtk

from aayra.components import AudioRow, AudioRowDialog
from aayra.constants import MimeType, String
from aayra.ext import Settings
from aayra.functions import (base_title, choose_file,
                             get_audio_thumbnail_bytes,
                             get_ebook_thumbnail_bytes, make_thumbnail,
                             make_title, set_row_props)
from aayra.models import Audio, Book


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/desk.ui")
class Desk(Adw.PreferencesPage):
    """Book editor and adder."""

    __gtype_name__ = "Desk"

    _book: Book
    _thumb_cache: dict[str, bytes] = {}
    factory = Gtk.Template.Child()
    title_row = Gtk.Template.Child()
    authors_row = Gtk.Template.Child()
    thumbnail_row = Gtk.Template.Child()
    thumbnail_pick_but = Gtk.Template.Child()
    ebook_row = Gtk.Template.Child()
    ebook_pick_but = Gtk.Template.Child()
    clear_but = Gtk.Template.Child()
    audiobooks_view = Gtk.Template.Child()
    audio_row_adder_but = Gtk.Template.Child()
    actions_group = Gtk.Template.Child()
    close_but = Gtk.Template.Child()
    save_but = Gtk.Template.Child()
    remove_but = Gtk.Template.Child()

    def __init__(self, book: Book = None):
        """Create a new desk for the given book."""
        super().__init__()
        self.book = book
        self.discoverer = GstPbutils.Discoverer()

        self.ebook_pick_but.connect("clicked", self.choose_ebook)
        self.thumbnail_pick_but.connect("clicked", self.choose_thumbnail)

        self.clear_but.connect("clicked", lambda _: self.clear_ebook())

        self.factory.connect("setup", AudioRow.setup)
        self.factory.connect("bind", self.bind_audio_row)
        self.factory.connect("unbind", self.unbind_audio_row)

        self.audio_row_adder_but.connect("clicked", self.add_audio_rows)
        self.audiobooks_view.connect("activate", self.edit_audio_row)

        self.close_but.connect("clicked", lambda _: self.emit("do-close"))
        self.save_but.connect("clicked", self.save_book)
        self.remove_but.connect("clicked", lambda _: self.emit("do-remove"))

    @GObject.Property(type=Book)
    def book(self):
        """Return current book."""
        return self._book

    @book.setter
    def book(self, book: Book = None):
        """Set current book."""
        self._book = book if book else Book()
        self._thumb_cache.clear()

        if self._book.id == -1:
            self.title_row.set_text("")
            self.authors_row.set_text("")
            self.thumbnail_row.set_title(String.DESK_THUMBNAIL_TITLE)
            self.thumbnail_row.set_subtitle(String.DESK_THUMBNAIL_SUBTITLE)
            self.actions_group.set_title("")
            self.actions_group.set_description("")
            self.remove_but.hide()
        else:
            self.title_row.set_text(self._book.title)
            self.authors_row.set_text(self._book.authors)
            self.set_thumbnail()
            self.actions_group.set_title(String.DESK_REMOVE_TITLE)
            self.actions_group.set_description(String.DESK_REMOVE_DESCRIPTION)
            self.remove_but.show()

        if self._book.ebook is None:
            self.clear_ebook()
        else:
            self.set_ebook()

        self.audiobooks_view.set_model(Gtk.NoSelection(model=self._book.audiobooks))

    @GObject.Signal(name="do-close")
    def _close(self):
        """Close button was clicked."""

    @GObject.Signal(name="do-remove")
    def _remove(self):
        """Remove button was clicked."""

    @GObject.Signal(name="do-save")
    def _save(self):
        """Save button was clicked."""

    def set_ebook(self):
        """Set eBook properties."""
        self.title_row.set_text(self._book.title)
        set_row_props(self.ebook_row, self._book.ebook)
        self.ebook_row.remove_css_class("error")
        self.clear_but.show()

    def clear_ebook(self):
        """Clear eBook."""
        self._book.ebook = None
        self.ebook_row.set_title(String.DESK_EBOOK_TITLE)
        self.ebook_row.set_subtitle(String.DESK_EBOOK_SUBTITLE)
        self.clear_but.hide()

    def set_thumbnail(self):
        """Set thumbnail properties."""
        set_row_props(self.thumbnail_row, self._book.thumbnail)

    def add_audio_rows(self, _):
        """Add audio rows."""

        def action(files):
            """Add audios to audiobooks."""
            idx = 0
            file = files.get_item(idx)
            while file is not None:
                path = file.get_path()
                duration = self.discover_audio(path)
                audio = Audio(title=base_title(path), path=path, duration=duration)
                self._book.audiobooks.append(audio)
                idx += 1
                file = files.get_item(idx)

        choose_file(
            action,
            String.FILE_DIALOG_TITLE_AUDIOS,
            [MimeType.M4B, MimeType.MP3],
            select_multiple=True,
        )

    def edit_audio_row(self, _, pos: int):
        """Edit an audio row."""
        audio = self._book.audiobooks.get_item(pos)
        dlg = AudioRowDialog(audio.copy())

        def save(_):
            """Save audio information."""
            dlg.audio.duration = self.discover_audio(dlg.audio.path)
            audio.merge(dlg.audio)
            dlg.destroy()

        def delete(_):
            """Delete audio."""
            res = self._book.audiobooks.find(audio)
            self._book.audiobooks.remove(res.position)
            dlg.destroy()

        dlg.connect("do-save", save)
        dlg.connect("do-delete", delete)
        dlg.present()

    def bind_audio_row(self, _: Gtk.ListView, litem: Gtk.ListItem):
        """Bind audio row."""
        audio = litem.get_item()
        AudioRow.bind(_, litem)
        self.audiobooks_view.show()
        self.ebook_row.remove_css_class("error")
        title = self.title_row.get_text()
        self._book.title = make_title(title, audio.path)
        self.title_row.set_text(self._book.title)

    def unbind_audio_row(self, _: Gtk.ListView, litem: Gtk.ListItem):
        """Unbind audio row."""
        if self._book.audiobooks.get_n_items() == 0:
            self.audiobooks_view.hide()

        AudioRow.unbind(_, litem)

    def choose_ebook(self, _):
        """Show file picker to choose eBook."""

        def action(file: Gio.File):
            """Store eBook file path."""
            path = file.get_path()
            self._book.ebook = path
            title = self.title_row.get_text().strip()
            self._book.title = make_title(title, path)
            self.set_ebook()

        choose_file(
            action, String.FILE_DIALOG_TITLE_EBOOK, [MimeType.PDF, MimeType.EPUB]
        )

    def choose_thumbnail(self, _):
        """Show file picker to choose thumbnail."""

        def action(file: Gio.File):
            """Store thumbnail file path."""
            path = file.get_path()
            self._book.thumbnail = path
            self.set_thumbnail()

        choose_file(
            action, String.FILE_DIALOG_TITLE_THUMBNAIL, [MimeType.JPEG, MimeType.PNG]
        )

    def discover_audio(self, path: str):
        """Discover audio file and update thumbnail and return duration."""
        info = self.discoverer.discover_uri(f"file://{path}")
        duration = info.get_duration() // Gst.SECOND

        if self._thumb_cache.get(path, None) is None:
            slist = info.get_stream_list()
            for sinfo in slist:
                tags = sinfo.get_tags()
                if tags:
                    thumb = get_audio_thumbnail_bytes(tags)
                    if thumb is not None:
                        self._thumb_cache[path] = thumb

        return duration

    def save_book(self, _):
        """Return True if details are valid."""
        n_audiobooks = self._book.audiobooks.get_n_items()
        if n_audiobooks == 0 and self._book.ebook is None:
            self.ebook_row.add_css_class("error")
            return

        title = self.title_row.get_text().strip()
        epath = self._book.ebook if self._book.ebook else ""
        audio = self._book.audiobooks.get_item(0)
        apath = audio.title if audio else ""
        self._book.title = make_title(title, epath or apath)

        authors = self.authors_row.get_text()
        self._book.authors = authors

        if self._book.ebook:
            thumb_bytes = get_ebook_thumbnail_bytes(self._book.ebook)
            self._book.thumbnail = make_thumbnail(thumb=thumb_bytes)
        elif self._book.thumbnail == f"{Settings.DATA_PATH}/fallback.jpg":
            for thumb_bytes in self._thumb_cache.values():
                self._book.thumbnail = make_thumbnail(thumb=thumb_bytes)
                break
        elif not self._book.thumbnail.startswith(Settings.STATE_PATH):
            self._book.thumbnail = make_thumbnail(path=self._book.thumbnail)
        else:
            pass

        self.emit("do-save")
