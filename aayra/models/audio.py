"""Contains Audio."""

from gi.repository import GObject


class Audio(GObject.Object):
    """Audio contains title and path to the audio."""

    _id: int
    _title: str
    _path: str
    _duration: int
    _progress: float

    def __init__(
        self,
        id: int = -1,
        title: str = "",
        path: str = "",
        duration: int = 0,
        progress: float = 0,
    ):
        """Create a new audio."""
        super().__init__()
        self._id = id
        self._title = title
        self._path = path
        self._duration = duration
        self._progress = progress

    def copy(self):
        """Create a copy of self."""
        audio = Audio(
            id=self._id,
            title=self._title,
            path=self._path,
            duration=self._duration,
            progress=self._progress,
        )
        return audio

    def merge(self, audio):
        """Merge audio to self."""
        self._id = audio.id
        self._title = audio.title
        self._path = audio.path
        self._duration = audio.duration
        self._progress = audio.progress
        self.notify("id")

    @GObject.Property(type=int)
    def id(self):
        """Get the ID of the audio."""
        return self._id

    @id.setter
    def id(self, id: int):
        """Set the ID of the audio."""
        self._id = id

    @GObject.Property(type=str)
    def title(self):
        """Get the title of the audio."""
        return self._title

    @title.setter
    def title(self, title: str):
        """Set the title of the audio."""
        self._title = title

    @GObject.Property(type=str)
    def path(self):
        """Get the path of the audio."""
        return self._path

    @path.setter
    def path(self, path: str):
        """Set the path of the audio."""
        self._path = path

    @GObject.Property(type=int)
    def duration(self):
        """Get the duration of the audio in seconds."""
        return self._duration

    @duration.setter
    def duration(self, duration: int):
        """Set the duration of the audio in seconds."""
        self._duration = duration

    @GObject.Property(type=float)
    def progress(self):
        """Get the progress in completion."""
        return self._progress

    @progress.setter
    def progress(self, progress: float):
        """Set the progress in completion."""
        self._progress = progress
