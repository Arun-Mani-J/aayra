"""Test aayra.AudioRowDialog."""

from aayra.components import Adw, AudioRowDialog, Gtk
from aayra.models import Audio


def on_activate(app):
    """On activate."""
    win = Adw.ApplicationWindow(application=app)
    win.present()
    audio = Audio(title="Hello", path="/path/to/hello.mp3", progress=0.6)
    dialog = AudioRowDialog(audio=audio)
    dialog.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
