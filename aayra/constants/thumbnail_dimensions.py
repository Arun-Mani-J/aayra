"""Contains ThumbnailDimensions."""

from enum import IntEnum


class ThumbnailDimensions(IntEnum):
    """Dimensions of a thumbnail."""

    WIDTH = 140
    HEIGHT = 94
