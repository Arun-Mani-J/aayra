"""Contains AudioRow."""

from gi.repository import Gtk

from aayra.ext import Settings
from aayra.models import Audio


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/audio_row.ui")
class AudioRow(Gtk.Box):
    """Action row to display an audio track."""

    __gtype_name__ = "AudioRow"

    title_lbl = Gtk.Template.Child()
    path_lbl = Gtk.Template.Child()
    _notify_signal_id: int

    @staticmethod
    def bind(_: Gtk.ListView, litem: Gtk.ListItem):
        """Bind to audio."""
        row = litem.get_child()
        audio = litem.get_item()
        row.fill(audio)

    def fill(self, audio: Audio):
        """Fill self with audio details."""
        self.title_lbl.set_text(audio.title)
        self.path_lbl.set_text(audio.path)
        self._notify_signal_id = audio.connect(
            "notify", lambda audio, _: self.fill(audio)
        )

    @staticmethod
    def setup(_: Gtk.ListView, litem: Gtk.ListItem):
        """Set up audio row."""
        row = AudioRow()
        litem.set_child(row)

    @staticmethod
    def unbind(_: Gtk.ListView, litem: Gtk.ListItem):
        """Unbind from audio."""
        row = litem.get_child()
        audio = litem.get_item()
        row.unfill(audio)

    def unfill(self, audio: Audio):
        """Unfill self from audio."""
        audio.disconnect(self._notify_signal_id)
