"""Contains AudioPlayer."""
import logging

from gi.repository import Gio, GLib, GObject, Gst, Gtk

from aayra.ext import Settings
from aayra.models import TocEntry


def format_duration(value: float):
    """Format the progress as duration."""
    hrs, mins, secs = (
        int(value // 3600),
        int((value % 3600) // 60),
        int((value % 3600) % 60),
    )
    return f"{hrs}:{mins}:{secs}"


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/audio_player.ui")
class AudioPlayer(Gtk.Box):
    """AudioPlayer has a basic audio player."""

    __gtype_name__ = "AudioPlayer"
    _audiobooks: Gio.ListStore | None = None
    _current_ix: int = 0
    _paused: bool = True
    adjustment_wid = Gtk.Template.Child()
    previous_but = Gtk.Template.Child()
    resume_but = Gtk.Template.Child()
    pause_but = Gtk.Template.Child()
    next_but = Gtk.Template.Child()
    timeline_scl = Gtk.Template.Child()
    volume_but = Gtk.Template.Child()

    def __init__(self, audiobooks: Gio.ListStore = None, play_from: int = 0):
        """Create a new audio player to play given audiobooks."""
        super().__init__()

        self.pipeline = Gst.ElementFactory.make(factoryname="playbin")
        self._audiobooks = audiobooks
        self._current_ix = play_from
        self.timeline_scl.set_format_value_func(lambda _, v: format_duration(v))

        value_changed_id = self.timeline_scl.connect("value-changed", self.do_seek)
        GLib.timeout_add_seconds(1, self.refresh, value_changed_id)

        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message", self.process)

        self.load_audio()

    @GObject.Property(type=Gio.ListStore)
    def audiobooks(self):
        """Get audiobooks."""
        return self._audiobooks

    @audiobooks.setter
    def audiobooks(self, audiobooks: Gio.ListStore | None):
        """Set audiobooks."""
        self._audiobooks = audiobooks
        self.load_audio()

    @GObject.Property(type=int)
    def current_ix(self):
        """Get current audiobook index."""
        return self._current_ix

    @current_ix.setter
    def current_ix(self, current_ix: int):
        """Set current audiobook index."""
        self._current_ix = current_ix
        self.load_audio()

    @GObject.Property(type=bool, default=True)
    def paused(self):
        """Get if the player is paused."""
        return self._paused

    @paused.setter
    def paused(self, paused: bool):
        """Set if the player is paused."""
        self._paused = paused

        if paused:
            self.pipeline.set_state(Gst.State.PAUSED)
            self.pause_but.hide()
            self.resume_but.show()
        else:
            self.pipeline.set_state(Gst.State.PLAYING)
            self.resume_but.hide()
            self.pause_but.show()

    @Gtk.Template.Callback()
    def change_volume(self, _, value: float):
        """Change volume of playback."""
        self.pipeline.set_property("volume", value)

    def close(self):
        """Close and free resources."""
        self.pipeline.set_state(Gst.State.NULL)

    def load_audio(self):
        """Load the audio to play."""
        autoplay = not self._paused
        self.paused = True

        if self._audiobooks is None:
            return

        audio = self._audiobooks.get_item(self._current_ix)
        if audio is None:
            if self._current_ix != 0:
                # Current playlist over, so go back to the last audiobook ix
                autoplay = False
                self._current_ix -= 1
                audio = self._audiobooks.get_item(self._current_ix)
            else:
                return

        self.pipeline.set_state(Gst.State.READY)
        self.pipeline.get_state(Gst.CLOCK_TIME_NONE)
        self.pipeline.set_property("uri", f"file://{audio.path}")
        self.adjustment_wid.set_upper(audio.duration)

        self.previous_but.set_sensitive(True)
        self.next_but.set_sensitive(True)

        if self._current_ix == 0:
            self.previous_but.set_sensitive(False)
        if self._current_ix == self._audiobooks.get_n_items() - 1:
            self.next_but.set_sensitive(False)

        logging.info("Loaded %s to play", audio.title)

        if autoplay:
            self.paused = False

    def get_toc(self):
        """Get table of contents."""
        return [
            TocEntry(self.audiobooks.get_item(i).title, str(i), "")
            for i in range(len(self.audiobooks))
        ]

    def play_entry(self, value: str):
        """Play selected entry from ToC."""
        self.current_ix = int(value)

    @Gtk.Template.Callback()
    def pause_playback(self, _):
        """Pause playing of audio."""
        self.paused = True

    @Gtk.Template.Callback()
    def play_previous(self, _):
        """Go to previous track."""
        self.current_ix -= 1

    @Gtk.Template.Callback()
    def play_next(self, _):
        """Go to next track."""
        self.current_ix += 1

    def process(self, _, msg):
        """Process message."""
        if msg.type == Gst.MessageType.EOS:
            self.paused = False
            self.current_ix += 1
        else:
            pass

    def refresh(self, sid):
        """Refresh the timeline."""
        if self._paused:
            return True

        with self.timeline_scl.handler_block(sid):
            val = self.pipeline.query_position(Gst.Format.TIME)[1] / Gst.SECOND
            self.timeline_scl.set_value(val)

        return True

    @Gtk.Template.Callback()
    def resume_playback(self, _):
        """Resume playing of audio."""
        self.paused = False

    def seek(self, val: float):
        """Call seek."""
        pos = val * Gst.SECOND
        self.pipeline.seek_simple(
            Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, pos
        )

    def do_seek(self, _):
        """Seek audio."""
        val = self.timeline_scl.get_value()
        self.seek(val)
