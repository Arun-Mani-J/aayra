"""Contains PageStore."""


import fitz

from aayra.constants import PagesView
from aayra.ext import Share


class Dimensions:
    """Simple structure to store dimensions."""

    width: float
    height: float

    def __init__(self, width: float, height: float):
        """Create a new Dimensions."""
        self.width = width
        self.height = height


class PageStore:
    """Store page and their metadata."""

    page_l: fitz.Page = None
    page_r: fitz.Page = None

    original = Dimensions(0, 0)
    scaled = Dimensions(0, 0)
    darea = Dimensions(0, 0)

    drect = fitz.Rect()

    selecting: bool = False
    selection_box: fitz.Rect = fitz.Rect()
    selected_text: str = ""

    def get_drawing_point(self, width: int, height: int):
        """Get the point to draw the pages on the canvas of given width and height."""
        if width > self.darea.width:
            x = (width - self.darea.width) / 2
        else:
            x = 0

        if height > self.darea.height:
            y = (height - self.darea.height) / 2
        else:
            y = 0

        self.drect.x0 = x
        self.drect.x1 = x + self.darea.width
        self.drect.y0 = y
        self.drect.y1 = y + self.darea.height
        return x, y

    def begin_selection(self, xval: float, yval: float):
        """Begin selection."""
        self.selection_box = fitz.Rect()
        if xval >= self.drect.x0:
            self.selection_box.x0 = xval
        else:
            self.selection_box.x0 = self.drect.x0

        if yval >= self.drect.y0:
            self.selection_box.y0 = yval
        else:
            self.selection_box.y0 = self.drect.y0

        self.selecting = True

    def update_selection(self, xval: float, yval: float):
        """Update selection."""
        self.selection_box.x1 = xval
        self.selection_box.y1 = yval
        self.selection_box.intersect(self.drect)

    def end_selection(self, xval: float, yval: float):
        """End selection."""
        self.update_selection(xval, yval)
        self.selecting = False

    def get_selected_text(self, pages_view: PagesView):
        """Obtain the text selected."""
        scale = Share.ZOOM_ADJUSTMENT.get_value() / 100
        box = fitz.Rect(
            self.selection_box.x0 - self.drect.x0,
            self.selection_box.y0 - self.drect.y0,
            self.selection_box.x0 - self.drect.x0 + self.selection_box.width,
            self.selection_box.y0 - self.drect.y0 + self.selection_box.height,
        )
        box /= scale

        if self.page_l:
            box_l = fitz.Rect(self.page_l.rect)
            box_l.intersect(box)
            text = self.page_l.get_textbox(box_l)
        else:
            text = ""

        if self.page_r:
            box.x0 = box.x0 - (self.drect.width / scale) + self.page_r.rect.width
            box.x1 = box.x0 + (self.selection_box.width / scale)
            box_r = fitz.Rect(self.page_r.rect)
            box_r.intersect(box)
            if text:
                text += "\n\n" + self.page_r.get_textbox(box_r)
            else:
                text = self.page_r.get_textbox(box_r)

        return text
