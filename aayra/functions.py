"""Miscellaneous functions."""

import mimetypes
import os
import tempfile
from typing import Callable

import fitz
from gi.repository import Adw, GdkPixbuf, Gio, GLib, Gst, Gtk

from aayra.constants import MimeType
from aayra.ext import Settings, Share
from aayra.models import Book


def base_title(path: str):
    """Get title-cased base name from path."""
    base = os.path.basename(path)
    title = base.split(".")[0].replace("_", " ").strip().title()
    return title


def choose_file(
    action: Callable[[Gio.File], None],
    title: str,
    mime_types: list[MimeType],
    transient_for: Gtk.Window = None,
    select_multiple: bool = False,
):
    """Show a file chooser with given title and for given mime types.

    It performs the given action after the files have been choosen.
    """

    def handle_response(dlg: Gtk.Dialog, res: int):
        """Handle response."""
        if res == Gtk.ResponseType.ACCEPT:
            if select_multiple:
                files = dlg.get_files()
                action(files)
            else:
                file = dlg.get_file()
                action(file)
        dlg.destroy()

    fltr = Gtk.FileFilter()
    for mime in mime_types:
        fltr.add_mime_type(mime.value)

    filechooser = Gtk.FileChooserDialog(
        title=title,
        action=Gtk.FileChooserAction.OPEN,
        filter=fltr,
        transient_for=transient_for
        if transient_for is not None
        else Share.PRIMARY_WINDOW,
        select_multiple=select_multiple,
    )
    filechooser.add_buttons(
        "Select", Gtk.ResponseType.ACCEPT, "Cancel", Gtk.ResponseType.CANCEL
    )
    filechooser.connect("response", handle_response)
    filechooser.show()


def get_audio_thumbnail_bytes(tags: Gst.TagList):
    """Get thumbnail of the audio from tag list."""
    exists, sample = tags.get_sample(Gst.TAG_IMAGE)

    if not exists:
        return None

    buf = sample.get_buffer()
    _, mem = buf.map(Gst.MapFlags.READ)
    return mem.data


def get_ebook_thumbnail_bytes(path: str):
    """Get the thumbnail of eBook."""
    doc = fitz.open(path)
    p = doc[0]
    px = p.get_pixmap()
    return px.tobytes(Settings.THUMBNAIL_EXT)


def make_thumbnail(
    path: str = None,
    thumb: bytes = None,
):
    """Make thumbnail from given bytes of image and return its path."""
    _, filename = tempfile.mkstemp(
        suffix=f".{Settings.THUMBNAIL_EXT}", dir=f"{Settings.STATE_PATH}/thumbnails"
    )

    def convert(name):
        """Convert given image to thumbnail type."""
        pbf = GdkPixbuf.Pixbuf.new_from_file(name)
        pbf.savev(filename, Settings.THUMBNAIL_EXT)

    if path is not None:
        convert(path)
    else:
        if thumb is not None:
            with tempfile.NamedTemporaryFile("wb") as raw:
                raw.write(thumb)
                convert(raw.name)
        else:
            raise ValueError("Either path or thumb must be provided")

    return filename


def make_title(text: str, path: str):
    """
    Make title from `text` or `path`.

    It will be stripped `text` if `text` is not empty else titled base name of `path`.
    """
    title = text.strip()

    if title:
        return title

    return base_title(path)


def match_book(book: Book, term: str):
    """Return if term matches book."""
    term = term.lower()
    return term in book.title.lower() or term in book.authors.lower()


def set_row_props(row: Adw.ActionRow, path: str):
    """Set title of row to basename of path and subtitle to path."""
    row.set_title(GLib.markup_escape_text(os.path.basename(path)))
    row.set_subtitle(GLib.markup_escape_text(path))
