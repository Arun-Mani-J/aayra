"""Contains Viewer."""

from gi.repository import Adw, GdkPixbuf, Gio, GObject, Gtk, Pango

from aayra.components import AudioPlayer, MuPdfReader
from aayra.constants import PagesView, ThumbnailDimensions
from aayra.ext import Settings, Share
from aayra.models import Book, TocEntry


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/viewer.ui")
class Viewer(Adw.Bin):
    """Viewer contains eBook reader and audiobook player."""

    __gtype_name__ = "Viewer"
    _book: Book | None = None
    audio_factory = Gtk.Template.Child()
    ebook_factory = Gtk.Template.Child()
    flap = Gtk.Template.Child()
    toc_sw = Gtk.Template.Child()
    toc_box = Gtk.Template.Child()
    ebook_expdr = Gtk.Template.Child()
    audio_expdr = Gtk.Template.Child()
    chapters_lv = Gtk.Template.Child()
    tracks_lv = Gtk.Template.Child()
    main_box = Gtk.Template.Child()
    body_box = Gtk.Template.Child()
    thumbnail_wid = Gtk.Template.Child()

    def __init__(self, book: Book = None):
        """Create a new viewer."""
        super().__init__()
        self.player = AudioPlayer()
        self.reader = MuPdfReader()
        self.book = book

        self.main_box.append(self.player)
        self.body_box.append(self.reader)

        Share.ZOOM_ADJUSTMENT.connect(
            "value-changed",
            lambda _: self.set_zoom_level(int(Share.ZOOM_ADJUSTMENT.get_value())),
        )

        self.audio_factory.connect("setup", self.setup_toc_label)
        self.audio_factory.connect("bind", self.bind_toc_label)

        self.ebook_factory.connect("setup", self.setup_toc_label)
        self.ebook_factory.connect("bind", self.bind_toc_label)

        self._ebook_tocs = Gio.ListStore(item_type=TocEntry)
        self.chapters_lv.set_model(Gtk.NoSelection(model=self._ebook_tocs))
        self.chapters_lv.connect("activate", self.read_selected_chapter)

        self._track_tocs = Gio.ListStore(item_type=TocEntry)
        self.tracks_lv.set_model(Gtk.NoSelection(model=self._track_tocs))
        self.tracks_lv.connect("activate", self.play_selected_track)

        self.flap.connect(
            "notify::reveal-flap",
            lambda *_: self.set_property("reveal-flap", self.flap.get_reveal_flap()),
        )

    @GObject.Property(type=Book)
    def book(self):
        """Get current book in viewer."""
        return self._book

    @book.setter
    def book(self, book: Book | None):
        """Set current book in viewer."""
        self._book = book

        if book is None:
            return

        pbf = GdkPixbuf.Pixbuf.new_from_file_at_size(
            book.thumbnail,
            -1,
            5 * ThumbnailDimensions.HEIGHT,
        )
        self.thumbnail_wid.set_pixbuf(pbf)
        self.thumbnail_wid.show()

        if book.ebook:
            self.ebook_expdr.show()
            self.ebook_expdr.set_expanded(True)
            self.reader.path = book.ebook
            self._ebook_tocs.remove_all()
            self._ebook_tocs.splice(0, 0, self.reader.get_toc())
            self.reader.show()
            self.thumbnail_wid.hide()
            self.reader.grab_focus()
        else:
            self.reader.hide()
            self.ebook_expdr.hide()
            self.thumbnail_wid.show()

        self.player.audiobooks = book.audiobooks
        self.player.current_ix = book.last_audiobook

        if book.audiobooks.get_n_items() == 0:
            self.player.hide()
            self.audio_expdr.hide()
        else:
            self._track_tocs.remove_all()
            self._track_tocs.splice(0, 0, self.player.get_toc())
            self.player.show()
            self.audio_expdr.show()
            self.audio_expdr.set_expanded(True)
            audio = book.audiobooks.get_item(book.last_audiobook)
            self.player.seek(audio.progress)  # TODO

        self.toc_sw.set_min_content_width(300)
        self.toc_sw.set_max_content_width(300)

    @GObject.Property(type=bool, default=False)
    def reveal_flap(self):
        """Get if flap is revealed."""
        return self.flap.get_reveal_flap()

    @reveal_flap.setter
    def reveal_flap(self, reveal: bool):
        """Set if flap should be revealed."""
        self.flap.set_reveal_flap(reveal)

    def set_pages_view(self, pages_view: PagesView):
        """Set pages view of reader."""
        self.reader.pages_view = pages_view.value

    def set_zoom_level(self, zoom_level: float):
        """Set zoom level of reader."""
        self.reader.zoom_level = zoom_level

    def set_invert_colors(self, invert: bool):
        """Set reader to invert page colors."""
        self.reader.invert_colors = invert

    # @Gtk.Template.Callback()
    def bind_toc_label(self, _, litem: Gtk.ListItem):
        """Bind ToC label."""
        child = litem.get_child()
        item = litem.get_item()
        tlabel = child.get_child_at(0, 0)
        tlabel.set_label(item.title)
        vlabel = child.get_child_at(1, 0)
        vlabel.set_label(item.label)

    # @Gtk.Template.Callback()
    def play_selected_track(self, _, ix: int):
        """Play selected track."""
        toc_entry = self._track_tocs.get_item(ix)
        self.player.play_entry(toc_entry.value)

    # @Gtk.Template.Callback()
    def read_selected_chapter(self, _, ix: int):
        """Read selected chapter."""
        toc_entry = self._ebook_tocs.get_item(ix)
        self.reader.read_entry(toc_entry.value)

    # @Gtk.Template.Callback()
    def setup_toc_label(self, _, litem: Gtk.ListItem):
        """Set up ToC label."""
        child = Gtk.Grid(valign=Gtk.Align.CENTER)
        child.attach(
            Gtk.Label(
                halign=Gtk.Align.START,
                hexpand=True,
                ellipsize=Pango.EllipsizeMode.MIDDLE,
                margin_end=5,
            ),
            0,
            0,
            1,
            1,
        )
        child.attach(Gtk.Label(halign=Gtk.Align.END), 1, 0, 1, 1)
        litem.set_child(child)

    def close(self):
        """Close the resources allocated."""
        if not self._book:
            return
        if self._book.ebook:
            self.reader.path = None
        if self._book.audiobooks.get_n_items() != 0:
            self.player.paused = True
            self.player.close()
