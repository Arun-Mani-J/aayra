"""Contains mime type constants."""

from enum import Enum


class MimeType(Enum):
    """Mime types for various files."""

    M4B = "audio/mp4"

    EPUB = "application/epub+zip"

    JPEG = "image/jpeg"

    MP3 = "audio/mp3"

    PDF = "application/pdf"

    PNG = "image/png"
