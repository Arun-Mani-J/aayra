"""Contains AudioRowDialog."""

from gi.repository import Adw, Gio, GObject, Gtk

from aayra.constants import MimeType, String
from aayra.ext import Settings, Share
from aayra.functions import choose_file, make_title, set_row_props
from aayra.models import Audio


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/audio_row_dialog.ui")
class AudioRowDialog(Adw.Window):
    """Show a dialog with entry for track name and file picker."""

    __gtype_name__ = "AudioRowDialog"

    save_but = Gtk.Template.Child()
    delete_but = Gtk.Template.Child()
    title_row = Gtk.Template.Child()
    path_row = Gtk.Template.Child()

    def __init__(self, audio: Audio):
        """Create a new dialog for given audio."""
        super().__init__(transient_for=Share.PRIMARY_WINDOW)
        self.audio = audio
        self.set_audio()

        self.delete_but.connect("clicked", lambda _: self.emit("do-delete"))
        self.save_but.connect("clicked", lambda _: self.save_audio())

    @GObject.Signal(name="do-delete")
    def _delete(self):
        """Delete button clicked."""

    @GObject.Signal(name="do-save")
    def _save(self):
        """Save button clicked."""

    def set_audio(self):
        """Set audio properties."""
        self.title_row.set_text(self.audio.title)
        set_row_props(self.path_row, self.audio.path)
        self.path_row.remove_css_class("error")

    @Gtk.Template.Callback()
    def close(self, _):
        """Close the dialog."""
        self.hide()
        self.destroy()

    @Gtk.Template.Callback()
    def choose_audio(self, _):
        """Show file picker to choose audio."""

        def action(file: Gio.File):
            """Store audio path."""
            path = file.get_path()
            self.audio.path = path
            self.audio.title = make_title(self.title_row.get_text(), path)
            self.set_audio()

        choose_file(
            action,
            String.FILE_DIALOG_TITLE_AUDIO,
            [MimeType.M4B, MimeType.MP3],
            transient_for=self,
            select_multiple=False,
        )

    def save_audio(self):
        """Return True if details are valid."""
        if not self.audio.path:
            self.path_row.add_css_class("error")
            return

        self.audio.title = make_title(self.title_row.get_text(), self.audio.path)
        self.emit("do-save")
