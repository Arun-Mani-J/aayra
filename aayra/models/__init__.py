"""Contains models to represent data."""

from .audio import Audio
from .book import Book
from .toc_entry import TocEntry
