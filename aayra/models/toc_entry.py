"""Contains TocEntry."""

from gi.repository import GObject


class TocEntry(GObject.Object):
    """TocEntry represents an entry in table of contents."""

    _title: str
    _value: str
    _label: str

    def __init__(self, title: str, value: str, label: str):
        """Create a new entry."""
        super().__init__()
        self._title = title.strip()
        self._value = value
        self._label = label.strip()

    @GObject.Property(type=str)
    def title(self):
        """Get title."""
        return self._title

    @GObject.Property(type=str)
    def value(self):
        """Get value."""
        return self._value

    @GObject.Property(type=str)
    def label(self):
        """Get label."""
        return self._label
