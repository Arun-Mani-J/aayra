"""Contains Aayra application."""

import logging

from gi.repository import Adw, Gdk, Gio, Gtk

from aayra.components import Window
from aayra.ext import Info, Settings, Share, Store


class Aayra(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        """Create Aayra application."""
        super().__init__(
            application_id=Info.ID,
            flags=Gio.ApplicationFlags.FLAGS_NONE,
        )
        self.store = Store()
        self.load_style()
        self.setup_actions()

    def do_activate(self):
        """Present main window."""
        Share.PRIMARY_WINDOW = Window(store=self.store, application=self)
        Share.PRIMARY_WINDOW.show_home()
        Share.PRIMARY_WINDOW.present()

    def load_style(self):
        """Load style."""
        provider = Gtk.CssProvider()
        provider.load_from_path(f"{Settings.DATA_PATH}/style.css")
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def setup_actions(self):
        """Set up actions."""
        donate_action = Gio.SimpleAction(name="donate")
        donate_action.connect("activate", lambda *_: logging.info("Donate clicked"))
        self.add_action(donate_action)

        about_action = Gio.SimpleAction(name="about")
        about_action.connect("activate", self.show_about)
        self.add_action(about_action)

    def show_about(self, *_):
        """Show about dialog."""
        dlg = Adw.AboutWindow(
            application_name=Info.NAME.value,
            developers=Info.DEVELOPERS.value,
            comments=Info.COMMENTS.value,
            copyright=Info.COPYRIGHT.value,
            issue_url=Info.ISSUE_URL.value,
            license_type=Info.LICENSE_TYPE.value,
            support_url=Info.SUPPORT_URL.value,
            version=Info.VERSION.value,
            website=Info.WEBSITE.value,
            transient_for=Share.PRIMARY_WINDOW,
        )
        dlg.present()

    def show_edit_dialog(self, *_):
        """Show edit dialog for active book."""
        Share.PRIMARY_WINDOW.show_edit_book_dialog()
