"""Components for the application."""

from gi.repository import Adw, Gio, Gtk

from .audio_player import AudioPlayer
from .audio_row import AudioRow
from .audio_row_dialog import AudioRowDialog
from .context_popover import ContextPopover
from .cover import Cover
from .desk import Desk
from .mupdf import MuPdfReader
from .reader import Reader
from .viewer import Viewer
from .window import Window
