PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS books(
       id INTEGER PRIMARY KEY,
       title TEXT NOT NULL,
       authors TEXT NOT NULL,
       ebook_path TEXT,
       thumbnail_path TEXT NOT NULL,
       progress REAL NOT NULL,
       last_opened TEXT NOT NULL,
       last_audiobook INTEGER
) STRICT;

CREATE TABLE IF NOT EXISTS audiobooks(
       id INTEGER PRIMARY KEY,
       bid INTEGER NOT NULL REFERENCES books(id) ON DELETE CASCADE,
       title TEXT NOT NULL,
       audio_path TEXT NOT NULL,
       duration INTEGER NOT NULL,
       progress REAL NOT NULL
) STRICT;
