"""Contains Cover."""

from gi.repository import GdkPixbuf, Gtk

from aayra.constants import ThumbnailDimensions
from aayra.ext import Settings
from aayra.models import Book


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/cover.ui")
class Cover(Gtk.Box):
    """Cover shows basic information of a book."""

    __gtype_name__ = "Cover"
    title = Gtk.Template.Child()
    authors = Gtk.Template.Child()
    progress = Gtk.Template.Child()
    thumbnail = Gtk.Template.Child()
    _notify_signal_id: int

    @staticmethod
    def bind(_: Gtk.GridView, litem: Gtk.ListItem):
        """Bind to book."""
        cover = litem.get_child()
        book = litem.get_item()
        cover.fill(book)

    def fill(self, book: Book):
        """Fill self with the book details."""
        self.progress.set_fraction(book.progress)
        self.title.set_label(book.title)
        self.authors.set_label(book.authors)

        pbf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            book.thumbnail, ThumbnailDimensions.WIDTH, ThumbnailDimensions.HEIGHT, True
        )
        self.thumbnail.set_pixbuf(pbf)
        self._notify_signal_id = book.connect("notify", lambda book, _: self.fill(book))

    @staticmethod
    def setup(_: Gtk.GridView, litem: Gtk.ListItem):
        """Set up cover."""
        cover = Cover()
        litem.set_child(cover)

    @staticmethod
    def unbind(_: Gtk.GridView, litem: Gtk.ListItem):
        """Unbind from book."""
        cover = litem.get_child()
        book = litem.get_item()
        cover.unfill(book)

    def unfill(self, book: Book):
        """Unfill book from self."""
        book.disconnect(self._notify_signal_id)
