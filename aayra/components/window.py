"""Main window of the app."""
import logging
from datetime import datetime, timezone

from gi.repository import Adw, Gio, GLib, Gtk

from aayra.components import Cover, Desk, Viewer
from aayra.constants import PagesView, String
from aayra.ext import Info, Settings, Share, Store
from aayra.functions import match_book
from aayra.models import Book


@Gtk.Template(filename=f"{Settings.DATA_PATH}/ui/window.ui")
class Window(Adw.ApplicationWindow):
    """Main window of the app."""

    __gtype_name__ = "Window"

    main = Gtk.Template.Child()
    menu_popover = Gtk.Template.Child()
    factory = Gtk.Template.Child()

    toc_but = Gtk.Template.Child()
    pages_spin_but = Gtk.Template.Child()
    home_but = Gtk.Template.Child()
    add_but = Gtk.Template.Child()
    title_wid = Gtk.Template.Child()
    search_but = Gtk.Template.Child()

    searchbar = Gtk.Template.Child()
    search_entry = Gtk.Template.Child()

    zoom_but = Gtk.Template.Child()
    zoom_best_fit_but = Gtk.Template.Child()
    zoom_level = Gtk.Template.Child()
    zoom_original_but = Gtk.Template.Child()

    pages_view_box = Gtk.Template.Child()
    invert_colors_button = Gtk.Template.Child()

    stack = Gtk.Template.Child()
    placeholder_page = Gtk.Template.Child()
    no_results_page = Gtk.Template.Child()
    library_view = Gtk.Template.Child()
    error_page = Gtk.Template.Child()

    def __init__(self, store: Store, application: Gtk.Application, **kwargs):
        """Create main window of the app."""
        super().__init__(application=application, **kwargs)
        self.store = store
        self.book: Book | None = None
        self.desk = Desk(book=None)
        self.viewer = Viewer()

        self.edit_action = Gio.SimpleAction(name="edit")
        self.edit_action.connect("activate", lambda *_: self.show_edit_book())

        self.pages_view_action = Gio.SimpleAction(
            name="switch_pages_view",
            state=GLib.Variant("s", "DUAL"),
            parameter_type=GLib.VariantType("s"),
        )
        self.pages_view_action.connect("change-state", self.toggle_pages_view)

        self.pages_spin_but.set_adjustment(Share.PAGES_ADJUSTMENT)
        self.zoom_level.set_adjustment(Share.ZOOM_ADJUSTMENT)

        self.invert_colors_button.connect("toggled", self.toggle_invert_colors)

        self.stack.add_child(self.desk)
        self.stack.add_child(self.viewer)

        self.desk.connect("do-close", lambda _: self.close_desk())
        self.desk.connect("do-save", lambda _: self.save_book())
        self.desk.connect("do-remove", lambda _: self.remove_book())

        self.home_but.connect("clicked", lambda _: self.show_home())
        self.toc_but.connect("toggled", lambda _: self.toggle_toc())
        self.add_but.connect("clicked", lambda _: self.show_add_book())
        self.search_but.connect("toggled", lambda _: self.toggle_searchbar())
        self.search_entry.connect(
            "search-changed", lambda _: self.update_library_filter()
        )
        self.library_view.connect("activate", lambda _, pos: self.open_book(pos))

        Share.ZOOM_ADJUSTMENT.connect(
            "value-changed",
            lambda _: self.zoom_but.set_label(
                f"{int(Share.ZOOM_ADJUSTMENT.get_value())}%"
            ),
        )
        self.zoom_best_fit_but.connect(
            "clicked", lambda _: Share.ZOOM_ADJUSTMENT.set_value(0)
        )
        self.zoom_original_but.connect(
            "clicked", lambda _: Share.ZOOM_ADJUSTMENT.set_value(100)
        )

        self.setup_actions()

        self.viewer.connect(
            "notify::reveal-flap",
            lambda *_: self.toc_but.set_active(self.viewer.reveal_flap),
        )

        self.factory.connect("bind", Cover.bind)
        self.factory.connect("setup", Cover.setup)
        self.factory.connect("unbind", Cover.unbind)

        self.search_filter = Gtk.CustomFilter()
        self.filter_model = Gtk.FilterListModel(
            filter=self.search_filter, model=self.store.list
        )
        self.search_filter.set_filter_func(match_book, "")

        self.library_view.set_model(Gtk.NoSelection(model=self.filter_model))
        # TODO GLib.timeout_add_seconds(settings.update_interval, self.update_recents)

    def setup_actions(self):
        """Set up actions."""
        self.add_action(self.pages_view_action)
        self.add_action(self.edit_action)

    def toggle_toc(self):
        """Toggle ToC."""
        self.viewer.reveal_flap = self.toc_but.get_active()

    def toggle_searchbar(self):
        """Toggle searchbar."""
        self.searchbar.set_search_mode(self.search_but.get_active())

    def toggle_pages_view(self, action: Gio.SimpleAction, pages_view: GLib.Variant):
        """Toggle pages view between single and dual."""
        action.set_state(pages_view)
        self.viewer.set_pages_view(PagesView(pages_view.get_string()))

    def toggle_invert_colors(self, _):
        """Toggle colors inversion in reader."""
        state = self.invert_colors_button.get_active()
        self.viewer.set_invert_colors(state)

    def update_library_filter(self):
        """Update library filter."""
        term = self.search_entry.get_text().strip()
        self.search_filter.set_filter_func(match_book, term)

        if self.filter_model.get_n_items() == 0:
            self.show_no_results_page()
        else:
            self.show_library()

    def open_book(self, pos: int):
        """Open a book at position."""
        book = self.store.list.get_item(pos)
        logging.info("Open %s", book.title)
        self.book = book
        self.show_viewer()

    def show_add_book(self):
        """Show add book page."""
        self.book = None
        self.desk.book = None
        self.show_desk()

    def show_edit_book(self):
        """Show edit book page."""
        self.desk.book = self.book.copy()
        self.show_desk()

    def show_desk(self):
        """Show desk page to add or edit book."""
        self.stack.set_visible_child(self.desk)
        self.add_but.hide()
        self.toc_but.hide()
        self.pages_spin_but.hide()
        self.home_but.hide()
        self.search_but.hide()
        self.searchbar.set_search_mode(False)
        self.zoom_but.hide()
        self.pages_view_box.hide()
        self.invert_colors_button.hide()
        self.edit_action.set_enabled(False)
        self.viewer.close()

        if self.book:
            self.title_wid.set_title(String.EDIT_BOOK)
            self.title_wid.set_subtitle("")
        else:
            self.title_wid.set_title(String.ADD_BOOK)
            self.title_wid.set_subtitle("")

        self.desk.show()

    def show_home(self):
        """Show home page which can be library or placeholder."""
        self.toc_but.hide()
        self.pages_spin_but.hide()
        self.home_but.hide()
        self.add_but.show()
        self.search_but.set_active(False)
        self.searchbar.set_search_mode(False)
        self.zoom_but.hide()
        self.title_wid.set_title(Info.NAME.value)
        self.title_wid.set_subtitle("")
        self.pages_view_box.hide()
        self.invert_colors_button.hide()
        self.edit_action.set_enabled(False)
        self.viewer.close()

        logging.info("Listing %s books", self.store.list.get_n_items())

        if self.store.list.get_n_items() == 0:
            self.show_placeholder()
        else:
            self.show_library()

    def show_library(self):
        """Show libary page."""
        self.search_but.show()
        self.stack.set_visible_child(self.library_view)

    def show_placeholder(self):
        """Show placeholder page."""
        self.search_but.hide()
        self.stack.set_visible_child(self.placeholder_page)

    def show_no_results_page(self):
        """Show no results page."""
        self.stack.set_visible_child(self.no_results_page)

    def show_viewer(self):
        """Show viewer page."""
        self.toc_but.show()
        self.home_but.show()
        self.add_but.hide()
        self.search_but.set_active(False)
        self.searchbar.set_search_mode(False)

        if self.book.ebook:
            self.pages_spin_but.show()
            self.search_but.show()
            self.zoom_but.show()
            self.pages_view_box.show()
            self.invert_colors_button.show()
        else:
            self.pages_spin_but.hide()
            self.search_but.hide()
            self.zoom_but.hide()
            self.pages_view_box.hide()
            self.invert_colors_button.hide()

        try:
            self.viewer.book = self.book
        except Exception as err:
            self.error_page.set_title(type(err).__name__)
            self.error_page.set_description(str(err))
            self.stack.set_visible_child(self.error_page)
        else:
            self.title_wid.set_title(self.book.title)
            self.title_wid.set_subtitle(self.book.authors)
            self.edit_action.set_enabled(True)
            self.stack.set_visible_child(self.viewer)
            self.toc_but.set_active(self.viewer.flap.get_reveal_flap())
            self.maximize()

    def close_desk(self):
        """Show the effective page."""
        self.desk.book = None
        if self.book:
            self.show_viewer()
        else:
            self.show_home()

    def save_book(self):
        """Save book details."""
        self.store.add_book(self.desk.book)
        if self.book is None:
            self.book = self.desk.book
        else:
            self.book.merge(self.desk.book)
        self.close_desk()

    def remove_book(self):
        """Remove book from store."""
        self.book.delete_thumbnail()
        self.store.remove_book(self.book)
        self.book = None
        self.close_desk()

    def update_recents(self):
        """Update recent information of book."""
        print("Start")
        if self.book is None or self.book.id == self.desk.book.id:
            return True
        print("Step")
        id = self.book.id
        last_opened = str(datetime.now(timezone.utc))
        last_audiobook = self.viewer.player.current_ix
        progress = 0  # TODO
        self.store.update_book_recent(id, last_opened, last_audiobook, progress)
        self.book.last_opened = last_opened
        self.book.last_audiobook = last_audiobook
        self.book.progress = progress

        if self.viewer.player.paused:
            return True

        audio = self.book.audiobooks.get_item(last_audiobook)
        if audio is not None:
            id = audio.id
            progress = self.viewer.player.timeline_scl.get_value()
            self.store.update_audio_recent(id, progress)
            audio.progress = progress

        print("Stop")
        return True
