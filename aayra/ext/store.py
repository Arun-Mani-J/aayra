"""Contains Store."""

import sqlite3
from datetime import datetime

from gi.repository import Gio

from aayra.ext import Settings
from aayra.models import Audio, Book


class Store:
    """Store maintains the books added by user.

    The data is persisted using SQLite.
    """

    def __init__(self, path: str = f"{Settings.STATE_PATH}/database.db"):
        """Create a new store.

        `path` is the location of the SQLite database.
        """
        self._db = sqlite3.connect(path, isolation_level=None)
        self.list = Gio.ListStore(item_type=Book)
        self.setup()
        self.fill()

    def setup(self):
        """Create the schema if it does not exist."""
        with open(f"{Settings.DATA_PATH}/schema.sql", encoding="utf-8") as script:
            self._db.executescript(script.read())

    def close(self):
        """Close the connection."""
        self._db.close()

    def fill(self):
        """Fill the list store with data from database."""
        cur = self._db.execute(
            """
            SELECT id, title, authors, ebook_path,
            thumbnail_path, progress, last_opened, last_audiobook
            FROM books;
            """
        )
        for row in cur:
            book = Book(
                id=row[0],
                title=row[1],
                authors=row[2],
                ebook=row[3],
                thumbnail=row[4],
                progress=row[5],
                last_opened=row[6],
                last_audiobook=row[7],
            )
            acur = self._db.execute(
                """SELECT id, title, audio_path, duration, progress
                   FROM audiobooks WHERE bid = :bid;""",
                {"bid": book.id},
            )
            for arow in acur:
                audio = Audio(
                    id=arow[0],
                    title=arow[1],
                    path=arow[2],
                    duration=arow[3],
                    progress=arow[4],
                )
                book.audiobooks.append(audio)

            self.list.append(book)

    def add_book(self, book: Book):
        """Add book to database."""
        cur = self._db.execute(
            """
            INSERT INTO books (id, title, authors, ebook_path,
            thumbnail_path, progress, last_opened, last_audiobook)
            VALUES (:id, :title, :authors, :ebook_path, :thumbnail_path,
                    :progress, :last_opened, :last_audiobook)
            ON CONFLICT (id) DO UPDATE SET
                title = :title, authors = :authors, ebook_path = :ebook_path,
                thumbnail_path = :thumbnail_path,
                progress = :progress, last_opened = :last_opened, last_audiobook = :last_audiobook
            RETURNING id;
            """,
            {
                "id": None if book.id == -1 else book.id,
                "title": book.title,
                "authors": book.authors,
                "ebook_path": book.ebook,
                "thumbnail_path": book.thumbnail,
                "progress": book.progress,
                "last_opened": book.last_opened,
                "last_audiobook": book.last_audiobook,
            },
        )
        (bid,) = next(cur)

        query = """
            INSERT INTO audiobooks (id, bid, title, audio_path, duration, progress)
            VALUES (:id, :bid, :title, :audio_path, :duration, :progress)
            ON CONFLICT (id) DO UPDATE SET
                title = :title, audio_path = :audio_path, duration = :duration, progress = :progress
            RETURNING id;
            """
        idx = 0
        audio = book.audiobooks.get_item(idx)

        while audio is not None:
            cur = self._db.execute(
                query,
                {
                    "id": None if audio.id == -1 else audio.id,
                    "bid": bid,
                    "title": audio.title,
                    "audio_path": audio.path,
                    "duration": audio.duration,
                    "progress": audio.progress,
                },
            )
            (id,) = next(cur)
            if audio.id == -1:
                audio.id = id

            idx += 1
            audio = book.audiobooks.get_item(idx)

        if book.id == -1:
            book.id = bid
            self.list.insert(0, book)

    def remove_book(self, book: Book):
        """Remove book from database."""
        self._db.execute("DELETE FROM books WHERE id = :id;", {"id": book.id})
        found, pos = self.list.find(book)

        if found:
            self.list.remove(pos)

    def update_book_recent(
        self,
        id: int,
        last_opened: datetime,
        last_audiobook: int,
        progress: float,
    ):
        """Update recenet information of a book."""
        self._db.execute(
            """
            UPDATE books SET last_opened=:last_opened, last_audiobook=:last_audiobook,
                progress=:progress WHERE id = :id;
            """,
            {
                "id": id,
                "last_opened": last_opened,
                "last_audiobook": last_audiobook,
                "progress": progress,
            },
        )

    def update_audio_recent(self, id: int, progress: float):
        """Update recent information of an audiobook."""
        self._db.execute(
            """
                UPDATE audiobooks SET progress = :progress
                WHERE id = :id;
                """,
            {"id": id, "progress": progress},
        )
