"""Aayra is an ebook reader and audiobook player."""

import logging

import gi

gi.require_version("Adw", "1")
gi.require_version("Gtk", "4.0")
gi.require_version("Gst", "1.0")
gi.require_version("GstPbutils", "1.0")
gi.require_version("WebKit2", "5.0")

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

from gi.repository import Gst

Gst.init([])
