"""Test aayra.Cover."""

from aayra.components import Adw, Cover


def on_activate(app):
    """On activate."""
    cover = Cover()
    win = Adw.ApplicationWindow(application=app)
    win.set_content(cover)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
