"""Test aayra.components.AudioPlayer."""

from os import getcwd
from random import random

from aayra.components import Adw, AudioPlayer, Gio
from aayra.models import Audio

audiobooks = Gio.ListStore(item_type=Audio)
for audio, dur in [
    ("Beat Thee.mp3", 191),
    ("Study and Relax.mp3", 223),
    ("The Celebrated Minuet.mp3", 216),
]:
    audiobooks.append(
        Audio(
            title=audio[:-4],
            path=f"{getcwd()}/tests/samples/{audio}",
            progress=random(),
            duration=dur,
        )
    )


def on_activate(app):
    """On activate."""
    player = AudioPlayer(audiobooks=audiobooks, play_from=0)
    win = Adw.ApplicationWindow(application=app)
    win.set_content(player)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
