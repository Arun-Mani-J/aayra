"""Contains MuPDF based reader."""

from .reader import MuPdfReader
