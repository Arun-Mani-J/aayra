"""Test aayra.AudioRow."""

from aayra.components import Adw, AudioRow, Gtk
from aayra.models import Audio


def on_activate(app):
    """On activate."""
    audio = Audio(title="Hello", path="/path/to/hello.mp3", progress=0.6)
    row = AudioRow()
    row.bind(audio)
    win = Adw.ApplicationWindow(application=app)
    win.set_content(row)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
