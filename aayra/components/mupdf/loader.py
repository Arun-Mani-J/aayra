"""Contains Loader."""
import functools
import io

import cairo
import fitz
from gi.repository import Gio

SIZE = 10


class Loader:
    """Loader takes care of fetching pages in advance and caching them."""

    def __init__(self, doc: fitz.Document = None, scale: float = 1.0):
        """Loader takes care of fetching pages in advance and caching them."""
        self._idx = 0
        self._doc = doc
        self._scale = scale
        self._matrix = fitz.Identity * scale
        self._invert_colors = False

    def _fetch(self, direction: int):
        """Fetch pages and load in cache in given direction."""
        if not self._doc or direction == 0:
            return

        if direction > 0:
            rng = range(self._idx, min(self._idx + SIZE // 2, len(self._doc)))
        else:
            rng = range(max(0, self._idx - SIZE // 2), self._idx)

        task = Gio.Task()
        task.run_in_thread(lambda *_: self._prefetch(rng))

    def _prefetch(self, rng: range):
        """Prefetch pages for cache."""
        for i in rng:
            self._load_page(i)

    @functools.lru_cache(maxsize=SIZE)
    def _load_page(self, idx: int):
        """Load and return the page pixmap."""
        page = self._doc[idx]
        pixmap = page.get_pixmap(matrix=self._matrix)

        if self._invert_colors:
            pixmap.invert_irect()

        fp = io.BytesIO(pixmap.tobytes())
        sfc = cairo.ImageSurface.create_from_png(fp)
        return page, sfc

    def get(self, idx: int):
        """Get the page of given index."""
        page, sfc = self._load_page(idx)
        direction = idx - self._idx
        self._idx = idx
        self._fetch(direction)
        return page, sfc

    @property
    def doc(self):
        """Return current document."""
        return self._doc

    @doc.setter
    def doc(self, doc: fitz.Document = None):
        """Set document."""
        self._doc = doc
        self._load_page.cache_clear()

    @property
    def scale(self):
        """Return scale factor of the pixmaps."""
        return self._scale

    @scale.setter
    def scale(self, scale: float):
        """Set new scale factor for the pixmaps."""
        self._scale = scale
        self._matrix = fitz.Identity * self._scale
        self._load_page.cache_clear()

    @property
    def invert_colors(self):
        """Get if colors are inverted."""
        return self._invert_colors

    @invert_colors.setter
    def invert_colors(self, invert: bool):
        """Set if colors are inverted."""
        self._invert_colors = invert
        self._load_page.cache_clear()
