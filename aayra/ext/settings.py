"""Contains Settings."""

from os import getcwd, getenv, makedirs

from aayra.constants import MimeType

CWD = getcwd()
HOME = getenv("AARYA_HOME")


class Settings:
    """Settings acts as a binding between various models of user data and configuration."""

    DEV = getenv("AAYRA_DEV", "0") == "1"
    DATA_PATH = (
        f"{CWD}/data" if DEV else getenv("XDG_DATA_HOME", f"{HOME}/.local/share")
    )
    STATE_PATH = (
        f"{CWD}/state" if DEV else getenv("XDG_STATE_HOME", f"{HOME}/.local/state")
    )
    THUMBNAIL_TYPE = MimeType.JPEG
    THUMBNAIL_EXT = "jpeg"
    UPDATE_INTERVAL = int(getenv("AARYA_UPDATE_INTERVAL", "5"))
    SEARCH_URL = getenv("AAYRA_SEARCH_URL", "https://duckduckgo.com?q={CONTENT}")
    SUMMARIZE_URL = getenv("AAYRA_SUMMARIZE_URL", "https://duckduckgo.com?q={CONTENT}")
    READ_URL = getenv("AAYRA_READ_URL", "https://duckduckgo.com?q={CONTENT}")


makedirs(Settings.DATA_PATH, exist_ok=True)
makedirs(f"{Settings.STATE_PATH}/thumbnails", exist_ok=True)
