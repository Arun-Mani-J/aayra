"""Contains Reader interface."""

from gi.repository import Adw, Gdk, GObject, Gtk

from aayra.components import ContextPopover
from aayra.constants import PagesView
from aayra.ext import Share


class Reader(Adw.Bin):
    """Reader displays eBook."""

    _pages_view: int
    _path: str | None
    _zoom_level: float
    _invert_colors: bool

    def __init__(
        self,
        page_num: int = 1,
        pages_view: int = PagesView.SINGLE.value,
        path: str = None,
        zoom_level: float = 100.0,
        invert_colors: bool = False,
    ):
        """Create a new reader."""
        super().__init__(vexpand=True, hexpand=True, can_focus=True)
        self.path = path
        self.pages_view = pages_view
        self.zoom_level = zoom_level
        self.invert_colors = invert_colors

        Share.PAGES_ADJUSTMENT.set_value(page_num)
        Share.PAGES_ADJUSTMENT.connect("value-changed", lambda _: self.do_load_page())

        self.popover = ContextPopover()
        self.popover.set_parent(self)

        event_ctrl = Gtk.EventControllerKey()
        self.add_controller(event_ctrl)
        event_ctrl.connect("key-pressed", self.process_key_press)

    def do_load_page(self):
        """Process current page."""
        raise NotImplementedError

    @GObject.Property(type=str, default=PagesView.SINGLE.value)
    def pages_view(self):
        """Get current pages view."""
        return self._pages_view

    @pages_view.setter
    def pages_view(self, pages_view: str):
        """Set current pages view."""
        self._pages_view = PagesView(pages_view)
        self.do_pages_view()

    def do_pages_view(self):
        """Process pages view."""
        raise NotImplementedError

    @GObject.Property(type=str)
    def path(self):
        """Get path to eBook."""
        return self._path

    @path.setter
    def path(self, path: str):
        """Set path to eBook."""
        self._path = path
        Share.PAGES_ADJUSTMENT.set_value(1)
        Share.ZOOM_ADJUSTMENT.set_value(100)
        if path:
            self.do_load_path()

    def do_load_path(self):
        """Load path."""
        raise NotImplementedError

    @GObject.Property(
        type=float, minimum=0.0, maximum=Share.ZOOM_ADJUSTMENT.get_upper()
    )
    def zoom_level(self):
        """Get zoom level for display."""
        return self._zoom_level

    @zoom_level.setter
    def zoom_level(self, zoom_level: float):
        """Set zoom level for display."""
        self._zoom_level = zoom_level
        self.do_zoom_level()

    def do_zoom_level(self):
        """Process zoom level."""
        raise NotImplementedError

    @GObject.Property(type=bool, default=False)
    def invert_colors(self):
        """Get if invert colors is enabled."""
        return self._invert_colors

    @invert_colors.setter
    def invert_colors(self, invert: bool):
        """Set if invert colors is enabled."""
        self._invert_colors = invert
        self.do_invert_colors()

    def do_invert_colors(self):
        """Process invert colors."""
        raise NotImplementedError

    def get_toc(self):
        """Get table of contents."""
        raise NotImplementedError

    def read_entry(self, value: str):
        """Read selected entry from ToC."""
        raise NotImplementedError

    def show_selected_popover(self, text: str, rect: Gdk.Rectangle):
        """Show selected context popover."""
        self.popover.selected_buffer.set_text(text)
        self.popover.set_pointing_to(rect)
        w = Share.PRIMARY_WINDOW.get_width()
        h = Share.PRIMARY_WINDOW.get_height()
        self.popover.set_size_request(w / 4, h / 4)
        self.popover.popup()

    def process_key_press(self, _, keyval: int, __, ___):
        """Process key press."""
        upper = Share.PAGES_ADJUSTMENT.get_upper()
        val = Share.PAGES_ADJUSTMENT.get_value()

        if keyval in (Gdk.KEY_Up, Gdk.KEY_Left):
            if val > 1:
                Share.PAGES_ADJUSTMENT.set_value(val - 1)
        elif keyval in (Gdk.KEY_Down, Gdk.KEY_Right):
            if val < upper:
                Share.PAGES_ADJUSTMENT.set_value(val + 1)
