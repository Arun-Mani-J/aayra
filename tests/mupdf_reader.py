"""Test aayra,components.MuPdfReader."""

from os import getcwd

from aayra.components import Adw, MuPdfReader


def on_activate(app):
    """On activate."""
    reader = MuPdfReader()
    reader.path = f"{getcwd()}/tests/samples/alice_in_wonderland.pdf"
    win = Adw.ApplicationWindow(application=app)
    win.set_content(reader)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
