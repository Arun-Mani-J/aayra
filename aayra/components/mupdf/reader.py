"""Contains MuPdfReader."""
import cairo
import fitz
from gi.repository import Gdk, Gtk

from aayra.components.reader import Reader
from aayra.constants import PagesView
from aayra.ext import Share
from aayra.models import TocEntry

from .loader import Loader
from .page_store import PageStore

GAP = 10


class MuPdfReader(Reader):
    """MuPDF based reader."""

    def __init__(
        self,
        page_num: int = 1,
        pages_view: int = PagesView.DUAL.value,
        path: str = None,
        zoom_level: float = 100.0,
    ):
        """Create a new MuPDF reader."""
        self._doc: fitz.Document = None
        self._loader = Loader()
        self._store = PageStore()

        self._darea = Gtk.DrawingArea()
        self._scrollw = Gtk.ScrolledWindow(
            margin_top=10, margin_end=10, margin_bottom=10, margin_start=10
        )

        self._gesture = Gtk.GestureSingle(button=1)

        self._gesture.connect("begin", lambda *_: self.begin_selection())
        self._gesture.connect("update", lambda *_: self.update_selection())
        self._gesture.connect("end", lambda *_: self.end_selection())

        super().__init__(
            page_num=page_num, pages_view=pages_view, path=path, zoom_level=zoom_level
        )

        self._scrollw.add_controller(self._gesture)
        self._scrollw.set_child(self._darea)
        self.set_child(self._scrollw)
        self._darea.set_draw_func(lambda _, cr, w, h: self._draw(cr, w, h))

    def _draw(self, cr: cairo.Context, width: int, height: int):
        """Draw current page in drawing area."""
        if not self._doc:
            return
        i = int(Share.PAGES_ADJUSTMENT.get_value()) - 1
        x, y = self._store.get_drawing_point(width, height)

        if self._pages_view == PagesView.SINGLE:
            self._store.page_l, sfc = self._loader.get(i)
            cr.set_source_surface(sfc, x, y)
            cr.paint()
        else:
            if i % 2 == 0:
                self._store.page_l, sfc_l = (
                    self._loader.get(i - 1) if i > 0 else (None, None)
                )
                self._store.page_r, sfc_r = self._loader.get(i)
            else:
                self._store.page_l, sfc_l = self._loader.get(i)
                self._store.page_r, sfc_r = (
                    self._loader.get(i + 1)
                    if i + 1 <= Share.PAGES_ADJUSTMENT.get_upper() - 1
                    else (None, None)
                )

            if sfc_l:
                cr.set_source_surface(sfc_l, x, y)
                cr.paint()
            if sfc_r:
                cr.set_source_surface(sfc_r, x + self._store.scaled.width + GAP, y)
                cr.paint()

        if self._store.selecting:
            cr.rectangle(
                self._store.selection_box.x0,
                self._store.selection_box.y0,
                self._store.selection_box.width,
                self._store.selection_box.height,
            )
            cr.set_source_rgb(0, 0, 255)
            cr.stroke()
            cr.rectangle(
                self._store.selection_box.x0,
                self._store.selection_box.y0,
                self._store.selection_box.width,
                self._store.selection_box.height,
            )
            cr.set_source_rgba(0, 0, 255, 0.25)
            cr.fill()

    def get_transformed_point(self):
        """Get the coordinates of touch adjusted for scrolling."""
        _, xval, yval = self._gesture.get_point()
        delx = self._scrollw.get_hadjustment().get_value()
        dely = self._scrollw.get_vadjustment().get_value()
        return xval + delx, yval + dely

    def begin_selection(self):
        """Begin selection of text."""
        xval, yval = self.get_transformed_point()
        self._store.begin_selection(xval, yval)
        self._darea.queue_draw()

    def update_selection(self):
        """Begin selection of text."""
        xval, yval = self.get_transformed_point()
        self._store.update_selection(xval, yval)
        self._darea.queue_draw()

    def end_selection(self):
        """Begin selection of text."""
        xval, yval = self.get_transformed_point()
        self._store.end_selection(xval, yval)
        text = self._store.get_selected_text(self._pages_view)
        x = (self._store.selection_box.x0 + self._store.selection_box.x1) / 2
        y = (self._store.selection_box.y0 + self._store.selection_box.y1) / 2
        rect = Gdk.Rectangle()
        rect.x = x
        rect.y = y
        rect.width = 10
        rect.height = 10
        self._darea.queue_draw()

        if text:
            self.show_selected_popover(text, rect)

    def update_measures(self):
        """Update drawing measures."""
        scale = self._zoom_level / 100

        self._loader.scale = scale
        self._store.scaled.width = self._store.original.width * scale
        self._store.scaled.height = self._store.original.height * scale

        if self._pages_view == PagesView.SINGLE:
            self._store.darea.width = self._store.scaled.width
        else:
            self._store.darea.width = self._store.scaled.width * 2 + GAP

        self._store.darea.height = self._store.scaled.height
        self._darea.set_content_width(self._store.darea.width)
        self._darea.set_content_height(self._store.darea.height)

    def do_load_path(self):
        """Load path."""
        self._doc = fitz.open(self.path)
        self._store.original.width = int(max((page.rect.width for page in self._doc)))
        self._store.original.height = int(max((page.rect.height for page in self._doc)))
        self._loader.doc = self._doc

        Share.PAGES_ADJUSTMENT.set_upper(len(self._doc))
        self.zoom_level = 100

    def do_load_page(self):
        """Process current page."""
        if not self._doc:
            return

        self.update_measures()
        self._darea.queue_draw()

    def do_pages_view(self):
        """Process pages view."""
        if self._pages_view == PagesView.SINGLE:
            Share.PAGES_ADJUSTMENT.set_step_increment(1)
        else:
            Share.PAGES_ADJUSTMENT.set_step_increment(2)

        self.do_load_page()

    def do_zoom_level(self):
        """Process zoom level."""
        if self._zoom_level == 0.0:
            width = self._scrollw.get_width()
            if self._pages_view == PagesView.DUAL:
                width = (width / 2) - GAP
            self._zoom_level = (width / self._store.original.width) * 100
            Share.ZOOM_ADJUSTMENT.set_value(self._zoom_level)

        self.do_load_page()

    def do_invert_colors(self):
        """Process invert colors."""
        self._loader.invert_colors = self._invert_colors
        self.do_load_page()

    def get_toc(self):
        """Get table of contents."""
        if not self._doc:
            return []
        return [
            TocEntry(title, value, str(value))
            for (lvl, title, value) in self._doc.get_toc()
            if lvl == 1
        ]

    def read_entry(self, value: str):
        """Read selected entry from ToC."""
        pno = int(value)
        Share.PAGES_ADJUSTMENT.set_value(pno)
