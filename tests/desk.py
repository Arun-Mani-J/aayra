"""Test aayra.Desk."""

from aayra.components import Adw, Desk


def on_activate(app):
    """On activate."""
    desk = Desk()
    win = Adw.ApplicationWindow(application=app)
    win.set_content(desk)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
