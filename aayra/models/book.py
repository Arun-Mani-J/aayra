"""Contains Book."""

import os
from datetime import datetime, timezone

from gi.repository import Gio, GObject

from aayra.ext import Settings
from aayra.models import Audio


class Book(GObject.Object):
    """Book represents a generic book.

    It contains the necessary information to locate and display a book.
    """

    _id: int
    _title: str
    _authors: str
    _ebook: str | None
    _audiobooks: Gio.ListStore
    _thumbnail: str
    _progress: float
    _last_opened: str
    _last_audiobook: int | None

    def __init__(
        self,
        id: int = -1,
        title: str = "",
        authors: str = "",
        ebook: str | None = None,
        audiobooks: Gio.ListStore = None,
        thumbnail: str = f"{Settings.DATA_PATH}/fallback.jpg",
        progress: float = 0,
        last_opened: datetime | None = None,
        last_audiobook: int = 0,
    ):
        """Create a new book. Either `ebook` or `audiobooks` must be provided."""
        super().__init__()
        self._id = id
        self._title = title
        self._authors = authors
        self._ebook = ebook
        self._audiobooks = audiobooks if audiobooks else Gio.ListStore(item_type=Audio)
        self._thumbnail = thumbnail
        self._progress = progress
        self._last_opened = (
            str(last_opened) if last_opened else str(datetime.now(timezone.utc))
        )
        self._last_audiobook = last_audiobook

    def copy(self):
        """Create a copy of self."""
        book = Book(
            id=self._id,
            title=self._title,
            authors=self._authors,
            ebook=self._ebook,
            thumbnail=self._thumbnail,
            progress=self._progress,
            last_opened=self._last_opened,
            last_audiobook=self._last_audiobook,
        )

        idx = 0
        audio = self._audiobooks.get_item(idx)

        while audio is not None:
            book.audiobooks.append(audio.copy())
            idx += 1
            audio = self._audiobooks.get_item(idx)

        return book

    def merge(self, book):
        """Merge book to self."""
        self._id = book.id
        self._title = book.title
        self._authors = book.authors
        self._ebook = book.ebook
        self._progress = book.progress
        self._last_opened = book.last_opened
        self._last_audiobook = book.last_audiobook

        if self._thumbnail != book.thumbnail:
            self.delete_thumbnail()
            self._thumbnail = book.thumbnail

        self._audiobooks.remove_all()
        idx = 0
        audio = book.audiobooks.get_item(idx)

        while audio is not None:
            self._audiobooks.append(audio.copy())
            idx += 1
            audio = book.audiobooks.get_item(idx)

        self.notify("id")

    def delete_thumbnail(self):
        """Delete thumbnail if it is not fallback."""
        if self._thumbnail != f"{Settings.DATA_PATH}/fallback.jpg":
            os.remove(self._thumbnail)

    @GObject.Property(type=int)
    def id(self):
        """Get the ID of the book."""
        return self._id

    @id.setter
    def id(self, id: int):
        """Set the ID of the book."""
        self._id = id

    @GObject.Property(type=str)
    def title(self):
        """Get the title of the book."""
        return self._title

    @title.setter
    def title(self, title: str):
        """Set the title of the book."""
        self._title = title

    @GObject.Property(type=str)
    def authors(self):
        """Get the authors of the book."""
        return self._authors

    @authors.setter
    def authors(self, authors: str):
        """Set the authors of the book."""
        self._authors = authors

    @GObject.Property(type=str)
    def ebook(self):
        """Get the path to ebook."""
        return self._ebook

    @ebook.setter
    def ebook(self, ebook: str | None):
        """Set the ebook path."""
        self._ebook = ebook

    @GObject.Property(type=Gio.ListStore, flags=GObject.ParamFlags.READABLE)
    def audiobooks(self):
        """Get the audiobooks."""
        return self._audiobooks

    @GObject.Property(type=str)
    def thumbnail(self):
        """Get the path to thumbnail."""
        return self._thumbnail

    @thumbnail.setter
    def thumbnail(self, thumbnail: str):
        """Set the path to thumbnail."""
        self._thumbnail = thumbnail

    @GObject.Property(type=float)
    def progress(self):
        """Get the progress in completion."""
        return self._progress

    @progress.setter
    def progress(self, progress: float):
        """Set the progress in completion."""
        self._progress = progress

    @GObject.Property(type=str)
    def last_opened(self):
        """Get the last opened date time."""
        return self._last_opened

    @last_opened.setter
    def last_opened(self, last_opened: str):
        """Set the last opened date time."""
        self._last_opened = str(datetime.fromisoformat(last_opened))

    @GObject.Property(type=int)
    def last_audiobook(self):
        """Get the last played audiobook index."""
        return self._last_audiobook

    @last_audiobook.setter
    def last_audiobook(self, last_audiobook: int | None):
        """Set the last played audiobook."""
        self._last_audiobook = last_audiobook
