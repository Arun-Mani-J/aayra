"""Global shareable widgets."""

from gi.repository import GLib, Gtk


class Share:
    """Global shareable widgets."""

    PRIMARY_WINDOW: Gtk.Window = None
    PAGES_ADJUSTMENT = Gtk.Adjustment(
        lower=1, upper=GLib.MAXINT32, step_increment=1, value=1
    )
    ZOOM_ADJUSTMENT = Gtk.Adjustment(lower=0, upper=400, step_increment=10, value=100)
