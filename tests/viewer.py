"""Test aayra.components.Viewer."""

from os import getcwd
from random import random

from aayra.components import Adw, Gio, Viewer
from aayra.models import Audio, Book

audiobooks = Gio.ListStore(item_type=Audio)
for audio, dur in [
    ("Beat Thee.mp3", 191),
    ("Study and Relax.mp3", 223),
    ("The Celebrated Minuet.mp3", 216),
]:
    audiobooks.append(
        Audio(
            title=audio[:-4],
            path=f"{getcwd()}/tests/samples/{audio}",
            progress=random(),
            duration=dur,
        )
    )
book = Book(
    id=10, title="John Doe", authors="Doe John", ebook="", audiobooks=audiobooks
)


def on_activate(app):
    """On activate."""
    viewer = Viewer(book=book)
    win = Adw.ApplicationWindow(application=app)
    win.set_content(viewer)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
