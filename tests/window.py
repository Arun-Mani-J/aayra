"""Test aayra.Window."""

from aayra.components import Adw, Window
from aayra.ext import Store


def on_activate(app):
    """On activate."""
    store = Store()
    win = Window(store, app)
    win.present()


app = Adw.Application(application_id="com.example.example")
app.connect("activate", on_activate)
app.run(None)
