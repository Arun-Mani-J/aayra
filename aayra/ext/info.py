"""Contains Info."""

from enum import Enum

from gi.repository import Gtk


class Info(Enum):
    """Information about the application."""

    ID = "org.aayra.aayra"
    NAME = "Aayra"
    DEVELOPERS = ["Arun Mani J", "Ashsui Marv", "Priyank"]
    COMMENTS = "Aayra lets you read and listen books"
    COPYRIGHT = "© Forever Aayra Developers"
    ISSUE_URL = "https://www.gitlab.com/arun_mani_j/aayra"
    LICENSE_TYPE = Gtk.License.GPL_3_0
    SUPPORT_URL = "https://www.gitlab.com/arun_mani_j/aayra"
    VERSION = "0.1 Alpha"
    WEBSITE = "https://www.gitlab.com/arun_mani_j/aayra"
